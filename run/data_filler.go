package run

import (
	"context"
	"fmt"
	"github.com/brianvoe/gofakeit/v6"
	"gitlab/library/internal/db/adapter"
	"gitlab/library/internal/models"
	"log"
)

// AuthorCount должно добавиться 10 авторов при старте приложения.
const AuthorCount = 10

// BookCount должно добавиться 100 книг при старте приложения. Книги должны ссылаться на существующего автора.
const BookCount = 100

// UserCount Количество пользователей — больше 50
const UserCount = 50

type DataFill struct {
	adapter *adapter.SQLAdapter
}

func NewDataFiller(sqlAdapter *adapter.SQLAdapter) *DataFill {
	return &DataFill{
		adapter: sqlAdapter,
	}
}

func (d *DataFill) LoadData() error {
	ctx := context.Background()

	authorIds, err := d.LoadAuthors(ctx, AuthorCount)
	if err != nil {
		log.Printf("LoadAuthors error: %v ", err)
	}

	err = d.LoadBooks(ctx, BookCount, authorIds)
	if err != nil {
		log.Printf("LoadBooks error: %v ", err)
	}
	err = d.LoadUsers(ctx, UserCount)
	if err != nil {
		log.Printf("LoadUsers error: %v ", err)
	}
	return err
}

func (d *DataFill) LoadAuthors(ctx context.Context, count int) (ids []int, err error) {
	list := make([]int, 0, count)

	seenAuthors := make(map[string]bool)
	author := models.Author{}

	for i := 0; i < count; i++ {

		for {
			author = d.GenerateAuthor()
			if _, ok := seenAuthors[author.Name]; !ok {
				seenAuthors[author.Name] = true
				break
			}
		}

		id, err := d.adapter.CreateWithID(ctx, &author)
		if err != nil {
			log.Printf("LoadAuthors error: %v ", err)
			return list, err
		}
		list = append(list, id)
	}

	return list, nil
}

func (d *DataFill) LoadBooks(ctx context.Context, count int, ids []int) error {
	seenBooks := make(map[string]bool)

	for i := 0; i < count; i++ {
		book := d.GenerateBook()

		for {
			if _, ok := seenBooks[book.Name]; !ok {
				seenBooks[book.Name] = true
				break
			}
			book = d.GenerateBook()
		}

		book.AuthorId = ids[gofakeit.Number(0, len(ids)-1)]

		err := d.adapter.Create(ctx, &book)
		if err != nil {
			log.Printf("LoadBooks error: %v ", err)
		}
	}
	return nil
}

func (d *DataFill) LoadUsers(ctx context.Context, count int) error {
	seenUsers := make(map[string]bool)

	for i := 0; i < count; i++ {

		user := d.GenerateUser()

		for {
			if _, ok := seenUsers[user.Username]; !ok {
				seenUsers[user.Username] = true
				break
			}
			user = d.GenerateUser()
		}

		err := d.adapter.Create(ctx, &user)

		if err != nil {
			return err
		}
	}
	return nil
}

func (d *DataFill) GenerateAuthor() models.Author {
	return models.Author{
		Name: gofakeit.BookAuthor(),
	}
}

func (d *DataFill) GenerateBook() models.Book {
	return models.Book{
		Name: gofakeit.BookTitle() + fmt.Sprintf(" %d", gofakeit.Number(1901, 2024)),
	}
}

func (d *DataFill) GenerateUser() models.User {
	return models.User{
		Username: gofakeit.Name(),
	}
}
