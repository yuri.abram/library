package models

type Author struct {
	ID    int    `json:"id,omitempty" db:"id" db_type:"SERIAL primary key"`
	Name  string `json:"name" db:"name" db_type:"varchar(255)" db_default:"not null" db_index:"index,unique" db_ops:"create,update" example:"Peter Parker"`
	Books []Book `json:"books,omitempty" db:"-"`
}

func (a *Author) TableName() string {
	return "author"
}

func (a *Author) OnCreate() []string {
	return []string{}
}

func (a *Author) GetID() int {
	return a.ID
}
