package models

type Book struct {
	ID         int    `json:"id,omitempty" db:"id" db_type:"SERIAL primary key"`
	Name       string `json:"name" db:"name" db_type:"varchar(255)" db_default:"not null" db_index:"index,unique" db_ops:"create,update" example:"The Lord of the Rings"`
	AuthorName string `json:"author_name,omitempty" db:"-"`
	AuthorId   int    `json:"author_id" db:"author_id" db_type:"integer" db_default:"not null" db_index:"index" db_ops:"create,update" example:"1"`
	Available  bool   `json:"available,omitempty" db:"available" db_type:"boolean" db_default:"default true" db_ops:"update"`
	UserId     *int   `json:"user_id,omitempty" db:"user_id" db_type:"integer" db_default:"default null" db_index:"index" db_ops:"update"`
}

func (b *Book) TableName() string {
	return "books"
}

func (b *Book) OnCreate() []string {
	return []string{}
}

func (b *Book) GetID() int {
	return b.ID
}

func (b *Book) GetAuthorId() int {
	return b.AuthorId
}

func (b *Book) IsAvailable() bool {
	return b.Available
}

func (b *Book) BookTheBook(id int) {
	b.UserId = &id
	b.Available = false
}

func (b *Book) ReturnTheBook() {
	b.UserId = nil
	b.Available = true
}

func (b *Book) IsRentedBy(idInt int) bool {
	return idInt == *b.UserId
}
