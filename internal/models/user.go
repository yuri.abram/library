package models

type User struct {
	ID          int    `json:"id,omitempty" db:"id" db_type:"SERIAL primary key"`
	Username    string `json:"username" db:"username" db_index:"index,unique" db_type:"VARCHAR(100)" db_default:"not null" db_ops:"create,update"`
	RentedBooks []Book `json:"rented_books,omitempty" db:"-"`
}

func (u *User) OnCreate() []string {
	return []string{}
}

func (u *User) TableName() string {
	return "users"
}

func (u *User) GetID() int {
	return u.ID
}
func (u *User) GetUsername() string {
	return u.Username
}

func (u *User) SetID(id int) {
	u.ID = id
}
func (u *User) SetUsername(username string) {
	u.Username = username
}
