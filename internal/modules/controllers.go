package modules

import (
	"gitlab/library/internal/infrastructure/component"
	libcontroller "gitlab/library/internal/modules/library/controller"
)

type Controllers struct {
	Lib libcontroller.LibController
}

func NewControllers(services *Services, components *component.Components) *Controllers {

	return &Controllers{
		Lib: libcontroller.NewLibCtl(services.Library, components),
	}
}
