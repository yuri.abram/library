package modules

import (
	"gitlab/library/internal/infrastructure/component"

	lservice "gitlab/library/internal/modules/library/service"
	aservice "gitlab/library/internal/modules/library/submodules/author/service"
	bservice "gitlab/library/internal/modules/library/submodules/book/service"
	uservice "gitlab/library/internal/modules/library/submodules/user/service"
)

type Services struct {
	Library lservice.LibServicer
}

func NewServices(storages *Storages, components *component.Components) *Services {
	author := aservice.NewAuthorService(storages.Author, components.Logger)
	book := bservice.NewBookService(storages.Book, components.Logger)
	user := uservice.NewUserService(storages.User, components.Logger)
	return &Services{
		Library: lservice.NewLibraryService(author, book, user, components.Logger),
	}
}
