package modules

import (
	"gitlab/library/internal/db/adapter"
	astorage "gitlab/library/internal/modules/library/submodules/author/storage"
	bstorage "gitlab/library/internal/modules/library/submodules/book/storage"
	ustorage "gitlab/library/internal/modules/library/submodules/user/storage"
)

type Storages struct {
	Author astorage.AuthorStorager
	Book   bstorage.BookStorager
	User   ustorage.UserStorager
}

func NewStorages(sqlAdapter *adapter.SQLAdapter) *Storages {
	return &Storages{
		Author: astorage.NewAuthorStorage(sqlAdapter),
		Book:   bstorage.NewBookStorage(sqlAdapter),
		User:   ustorage.NewUserStorage(sqlAdapter),
	}
}
