package service

import (
	"context"
	"gitlab/library/internal/models"
	"gitlab/library/internal/modules/library/submodules/user/storage"
	"go.uber.org/zap"
)

type UserServicer interface {
	CreateUser(ctx context.Context, user models.User) error
	GetAllUsers(ctx context.Context) (out []models.User, err error)
	UpdateUser(ctx context.Context, user models.User) error
	GetUserByID(ctx context.Context, id int) (out models.User, err error)
}

type UserService struct {
	storage storage.UserStorager
	logger  *zap.Logger
}

func NewUserService(storage storage.UserStorager, logger *zap.Logger) *UserService {
	return &UserService{
		storage: storage,
		logger:  logger,
	}
}

func (us *UserService) CreateUser(ctx context.Context, user models.User) error {
	return us.storage.Create(ctx, user)
}

func (us *UserService) GetAllUsers(ctx context.Context) ([]models.User, error) {
	return us.storage.List(ctx)
}

func (us *UserService) UpdateUser(ctx context.Context, user models.User) error {
	return us.storage.Create(ctx, user)
}

func (us *UserService) GetUserByID(ctx context.Context, id int) (models.User, error) {
	return us.storage.GetByID(ctx, id)
}
