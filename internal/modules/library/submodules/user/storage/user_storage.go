package storage

import (
	"context"
	sq "github.com/Masterminds/squirrel"
	"gitlab/library/internal/db/adapter"
	"gitlab/library/internal/infrastructure/db/scanner"
	"gitlab/library/internal/models"
)

type UserStorager interface {
	Create(ctx context.Context, user models.User) error
	List(ctx context.Context) (out []models.User, err error)
	Update(ctx context.Context, user models.User) error
	GetByID(ctx context.Context, id int) (out models.User, err error)
}

type UserStorage struct {
	adapter adapter.SQLAdapter
}

func NewUserStorage(adapter *adapter.SQLAdapter) *UserStorage {
	return &UserStorage{
		adapter: *adapter,
	}
}

func (u *UserStorage) Create(ctx context.Context, user models.User) error {
	return u.adapter.Create(ctx, &user)
}

func (u *UserStorage) List(ctx context.Context) ([]models.User, error) {
	var users []models.User
	err := u.adapter.List(ctx, &users, "users", adapter.Condition{})

	for i, user := range users {
		var books []models.Book
		err = u.adapter.List(ctx, &books, "books", adapter.Condition{
			Equal: map[string]interface{}{
				"user_id": user.GetID(),
			},
		})
		if err != nil {
			return nil, err
		}
		users[i].RentedBooks = books
	}
	return users, err
}

func (u *UserStorage) Update(ctx context.Context, user models.User) error {
	return u.adapter.Update(ctx, &user, adapter.Condition{
		Equal: sq.Eq{
			"id": user.GetID(),
		},
	}, scanner.Update)
}

func (u *UserStorage) GetByID(ctx context.Context, id int) (models.User, error) {
	var users []models.User
	err := u.adapter.List(ctx, &users, "users", adapter.Condition{
		Equal: map[string]interface{}{
			"id": id,
		},
	})
	return users[0], err
}
