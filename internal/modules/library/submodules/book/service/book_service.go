package service

import (
	"context"
	"gitlab/library/internal/models"
	"gitlab/library/internal/modules/library/submodules/book/storage"
	"go.uber.org/zap"
)

type BookServicer interface {
	CreateBook(ctx context.Context, book models.Book) error
	CreateWithId(ctx context.Context, book models.Book) (id int, err error)
	GetAllBooks(ctx context.Context) (out []models.Book, err error)
	UpdateBook(ctx context.Context, book models.Book) error
	GetBookByID(ctx context.Context, id int) (out models.Book, err error)
}

type BookService struct {
	storage storage.BookStorager
	logger  *zap.Logger
}

func NewBookService(storage storage.BookStorager, logger *zap.Logger) *BookService {
	return &BookService{
		storage: storage,
		logger:  logger,
	}
}

func (b *BookService) CreateBook(ctx context.Context, book models.Book) error {
	return b.storage.Create(ctx, book)
}

func (b *BookService) CreateWithId(ctx context.Context, book models.Book) (int, error) {
	return b.storage.CreateWithID(ctx, book)
}
func (b *BookService) GetAllBooks(ctx context.Context) ([]models.Book, error) {
	return b.storage.List(ctx)
}

func (b *BookService) UpdateBook(ctx context.Context, book models.Book) error {
	return b.storage.Update(ctx, book)
}

func (b *BookService) GetBookByID(ctx context.Context, id int) (models.Book, error) {
	return b.storage.GetByID(ctx, id)
}
