package storage

import (
	"context"
	sq "github.com/Masterminds/squirrel"
	"gitlab/library/internal/db/adapter"
	"gitlab/library/internal/infrastructure/db/scanner"
	"gitlab/library/internal/models"
)

type BookStorager interface {
	Create(ctx context.Context, book models.Book) error
	CreateWithID(ctx context.Context, book models.Book) (id int, err error)
	List(ctx context.Context) (out []models.Book, err error)
	Update(ctx context.Context, book models.Book) error
	GetByID(ctx context.Context, id int) (out models.Book, err error)
}

type BookStorage struct {
	adapter adapter.SQLAdapter
}

func NewBookStorage(adapter *adapter.SQLAdapter) *BookStorage {
	return &BookStorage{
		adapter: *adapter,
	}
}

func (b *BookStorage) Create(ctx context.Context, book models.Book) error {
	return b.adapter.Create(ctx, &book)
}

func (b *BookStorage) CreateWithID(ctx context.Context, book models.Book) (int, error) {
	return b.adapter.CreateWithID(ctx, &book)
}

func (b *BookStorage) List(ctx context.Context) ([]models.Book, error) {
	var books []models.Book
	err := b.adapter.List(ctx, &books, "books", adapter.Condition{})

	for i, book := range books {
		var authors []models.Author
		err = b.adapter.List(ctx, &authors, "author", adapter.Condition{
			Equal: map[string]interface{}{
				"id": book.GetAuthorId(),
			},
		})
		if err != nil {
			return nil, err
		}
		books[i].AuthorName = authors[0].Name
	}

	return books, err
}

func (b *BookStorage) Update(ctx context.Context, book models.Book) error {
	return b.adapter.Update(ctx, &book, adapter.Condition{
		Equal: sq.Eq{
			"id": book.GetID(),
		},
	}, scanner.Update)
}

func (b *BookStorage) GetByID(ctx context.Context, id int) (models.Book, error) {
	var books []models.Book
	err := b.adapter.List(ctx, &books, "books", adapter.Condition{
		Equal: map[string]interface{}{
			"id": id,
		},
	})
	return books[0], err
}
