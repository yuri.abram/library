package storage

import (
	"context"
	"fmt"
	"gitlab/library/internal/db/adapter"
	"gitlab/library/internal/infrastructure/db/scanner"
	"gitlab/library/internal/models"
)

type AuthorStorager interface {
	Create(ctx context.Context, author models.Author) error
	List(ctx context.Context) (out []models.Author, err error)
	GetByID(ctx context.Context, id int) (out models.Author, err error)
	Update(ctx context.Context, author models.Author) error
}

type AuthorStorage struct {
	adapter adapter.SQLAdapter
}

func NewAuthorStorage(adapter *adapter.SQLAdapter) *AuthorStorage {
	return &AuthorStorage{
		adapter: *adapter,
	}
}

func (a *AuthorStorage) Create(ctx context.Context, author models.Author) error {
	return a.adapter.Create(ctx, &author)
}

func (a *AuthorStorage) List(ctx context.Context) ([]models.Author, error) {
	var authors []models.Author
	err := a.adapter.List(ctx, &authors, "author", adapter.Condition{})
	if err != nil {
		return nil, err
	}

	for i, author := range authors {
		var books []models.Book
		err = a.adapter.List(ctx, &books, "books", adapter.Condition{
			Equal: map[string]interface{}{
				"author_id": author.ID,
			},
		})
		if err != nil {
			return nil, err
		}
		authors[i].Books = books
	}

	return authors, err
}

func (a *AuthorStorage) GetByID(ctx context.Context, id int) (models.Author, error) {
	var authors []models.Author

	err := a.adapter.List(ctx, &authors, "author", adapter.Condition{
		Equal: map[string]interface{}{
			"id": id,
		},
	})
	if err != nil {
		return models.Author{}, err
	}
	if len(authors) == 0 {
		return models.Author{}, fmt.Errorf("author with id %d not found", id)
	}

	var books []models.Book
	err = a.adapter.List(ctx, &books, "books", adapter.Condition{
		Equal: map[string]interface{}{
			"author_id": authors[0].ID,
		},
	})
	if err != nil {
		return models.Author{}, err
	}
	authors[0].Books = books
	return authors[0], err
}

func (a *AuthorStorage) Update(ctx context.Context, author models.Author) error {
	err := a.adapter.Update(ctx, &author, adapter.Condition{Equal: map[string]interface{}{
		"id": author.GetID(),
	}, ForUpdate: true,
	},
		scanner.Update)

	if err != nil {
		return fmt.Errorf("update error: %w", err)
	}

	return err
}
