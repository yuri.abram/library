package service

import (
	"context"
	"gitlab/library/internal/models"
	"gitlab/library/internal/modules/library/submodules/author/storage"
	"go.uber.org/zap"
)

type AuthorServicer interface {
	CreateAuthor(ctx context.Context, author models.Author) error
	GetAllAuthors(ctx context.Context) (out []models.Author, err error)
	UpdateAuthor(ctx context.Context, author models.Author) error
	GetAuthorByID(ctx context.Context, id int) (out models.Author, err error)
}

type AuthorService struct {
	storage storage.AuthorStorager
	logger  *zap.Logger
}

func NewAuthorService(storage storage.AuthorStorager, logger *zap.Logger) *AuthorService {
	return &AuthorService{
		storage: storage,
		logger:  logger,
	}
}

func (a *AuthorService) CreateAuthor(ctx context.Context, author models.Author) error {
	return a.storage.Create(ctx, author)
}

func (a *AuthorService) GetAllAuthors(ctx context.Context) ([]models.Author, error) {
	return a.storage.List(ctx)
}

func (a *AuthorService) UpdateAuthor(ctx context.Context, author models.Author) error {
	return a.storage.Update(ctx, author)
}

func (a *AuthorService) GetAuthorByID(ctx context.Context, id int) (models.Author, error) {
	return a.storage.GetByID(ctx, id)
}
