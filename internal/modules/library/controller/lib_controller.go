package controller

import (
	"encoding/json"
	"gitlab/library/internal/infrastructure/component"
	"gitlab/library/internal/infrastructure/responder"
	"gitlab/library/internal/models"
	"gitlab/library/internal/modules/library/service"
	"net/http"
)

type LibController interface {
	AddAuthor(w http.ResponseWriter, r *http.Request)
	GetAuthors(w http.ResponseWriter, r *http.Request)
	AddBook(w http.ResponseWriter, r *http.Request)
	GetBooks(w http.ResponseWriter, r *http.Request)
	AddUser(w http.ResponseWriter, r *http.Request)
	GetUsers(w http.ResponseWriter, r *http.Request)
	RentBook(w http.ResponseWriter, r *http.Request)
	ReturnBook(w http.ResponseWriter, r *http.Request)
}

type Library struct {
	service   service.LibServicer
	responder responder.Responder
}

func NewLibCtl(service service.LibServicer, components *component.Components) *Library {
	return &Library{
		service:   service,
		responder: components.Responder,
	}
}

// AddAuthor
// @Summary Add author
// @Tags library
// @Accept json
// @Produce json
// @Param author body AuthorRequest true "Author"
// @Success 200
// @Failure 500
// @Router /library/author [post]
func (l *Library) AddAuthor(w http.ResponseWriter, r *http.Request) {
	var rq AuthorRequest
	err := json.NewDecoder(r.Body).Decode(&rq)
	if err != nil {
		l.responder.ErrorBadRequest(w, err)
		return
	}

	author := models.Author{
		Name: rq.Name,
	}

	err = l.service.CreateAuthor(r.Context(), author)
	if err != nil {
		l.responder.ErrorInternal(w, err)
		return
	}
	l.responder.OutputJSON(w, "Success")
}

// GetAuthors
// @Summary Get all authors
// @Tags library
// @Accept json
// @Produce json
// @Success 200 {array} []models.Author
// @Router /library/author [get]
func (l *Library) GetAuthors(w http.ResponseWriter, r *http.Request) {
	authors, err := l.service.GetAllAuthors(r.Context())
	if err != nil {
		l.responder.ErrorInternal(w, err)
		return
	}
	l.responder.OutputJSON(w, authors)
}

// AddBook
// @Summary Add book
// @Tags library
// @Accept json
// @Produce json
// @Param book body AddBookRequest true "Book"
// @Success 200
// @Failure 500
// @Router /library/book [post]
func (l *Library) AddBook(w http.ResponseWriter, r *http.Request) {
	var bookRq AddBookRequest
	err := json.NewDecoder(r.Body).Decode(&bookRq)
	if err != nil {
		l.responder.ErrorBadRequest(w, err)
		return
	}

	book := models.Book{
		Name:      bookRq.Title,
		AuthorId:  bookRq.AuthorID,
		Available: true,
	}

	err = l.service.CreateBook(r.Context(), book)
	if err != nil {
		l.responder.ErrorInternal(w, err)
		return
	}
	l.responder.OutputJSON(w, "Success")
}

// GetBooks
// @Summary Get all books
// @Tags library
// @Accept json
// @Produce json
// @Success 200 {array} models.Book
// @Failure 500
// @Router /library/book [get]
func (l *Library) GetBooks(w http.ResponseWriter, r *http.Request) {
	books, err := l.service.GetAllBooks(r.Context())
	if err != nil {
		l.responder.ErrorInternal(w, err)
		return
	}
	l.responder.OutputJSON(w, books)
}

// AddUser
// @Summary Add user
// @Tags library
// @Accept json
// @Produce json
// @Param user body AddUserRequest true "User"
// @Success 200
// @Failure 500
// @Router /library/user [post]
func (l *Library) AddUser(w http.ResponseWriter, r *http.Request) {
	var userRq AddUserRequest
	err := json.NewDecoder(r.Body).Decode(&userRq)
	if err != nil {
		l.responder.ErrorBadRequest(w, err)
		return
	}

	user := models.User{
		Username: userRq.Name,
	}

	err = l.service.CreateUser(r.Context(), user)
	if err != nil {
		l.responder.ErrorInternal(w, err)
		return
	}
	l.responder.OutputJSON(w, "Success")
}

// GetUsers
// @Summary Get all users
// @Tags library
// @Accept json
// @Produce json
// @Success 200 {array} models.User
// @Failure 500
// @Router /library/user [get]
func (l *Library) GetUsers(w http.ResponseWriter, r *http.Request) {
	users, err := l.service.GetAllUsers(r.Context())
	if err != nil {
		l.responder.ErrorInternal(w, err)
		return
	}
	l.responder.OutputJSON(w, users)
}

// RentBook
// @Summary Rent book
// @Tags library
// @Accept json
// @Produce json
// @Param rent body BookRentRequest true "Rent"
// @Success 200
// @Failure 500
// @Failure 400
// @Router /library/rent [post]
func (l *Library) RentBook(w http.ResponseWriter, r *http.Request) {
	var rentRq BookRentRequest
	err := json.NewDecoder(r.Body).Decode(&rentRq)
	if err != nil {
		l.responder.ErrorBadRequest(w, err)
		return
	}

	err = l.service.RentBook(r.Context(), rentRq.BookId, rentRq.UserId)
	if err != nil {
		l.responder.ErrorInternal(w, err)
		return
	}
	l.responder.OutputJSON(w, "Success")
}

// ReturnBook
// @Summary Return book
// @Tags library
// @Accept json
// @Produce json
// @Param rent body BookRentRequest true "Rent"
// @Success 200
// @Router /library/return [post]
func (l *Library) ReturnBook(w http.ResponseWriter, r *http.Request) {
	var rentRq BookRentRequest
	err := json.NewDecoder(r.Body).Decode(&rentRq)
	if err != nil {
		l.responder.ErrorBadRequest(w, err)
		return
	}

	err = l.service.ReturnBook(r.Context(), rentRq.BookId, rentRq.UserId)
	if err != nil {
		l.responder.ErrorInternal(w, err)
		return
	}
	l.responder.OutputJSON(w, "Success")
}
