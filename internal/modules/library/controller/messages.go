package controller

// BookRentRequest
// @Summary Rent a book request
// @Description Rent a book
// @Tags library
type BookRentRequest struct {
	BookId string `json:"bookId" example:"1"`
	UserId string `json:"userId" example:"1"`
}

// AuthorRequest
// @Summary Add author request
// @Tags library
type AuthorRequest struct {
	Name string `json:"name" example:"J. R. R. Tolkien"`
}

// AddBookRequest
// @Summary Add book request
// @Tags library
type AddBookRequest struct {
	Title    string `json:"title" example:"The Lord of the Rings"`
	AuthorID int    `json:"authorId" example:"1"`
}

// AddUserRequest
// @Summary Add user request
// @Tags library
type AddUserRequest struct {
	Name string `json:"name" example:"Vasya Pupkin"`
}
