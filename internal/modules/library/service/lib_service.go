package service

import (
	"context"
	"fmt"
	"gitlab/library/internal/models"
	"go.uber.org/zap"
	"strconv"

	aservice "gitlab/library/internal/modules/library/submodules/author/service"
	bservice "gitlab/library/internal/modules/library/submodules/book/service"
	uservice "gitlab/library/internal/modules/library/submodules/user/service"
)

type LibServicer interface {
	CreateAuthor(ctx context.Context, author models.Author) error
	CreateUser(ctx context.Context, user models.User) error
	CreateBook(ctx context.Context, book models.Book) error
	RentBook(ctx context.Context, bookID, userID string) error
	ReturnBook(ctx context.Context, bookID, userID string) error
	GetAllAuthors(ctx context.Context) (out []models.Author, err error)
	GetAllUsers(ctx context.Context) (out []models.User, err error)
	GetAllBooks(ctx context.Context) (out []models.Book, err error)
}

type LibService struct {
	Author aservice.AuthorServicer
	Book   bservice.BookServicer
	User   uservice.UserServicer
	logger *zap.Logger
}

func NewLibraryService(author aservice.AuthorServicer, book bservice.BookServicer, user uservice.UserServicer, logger *zap.Logger) *LibService {
	return &LibService{
		Author: author,
		Book:   book,
		User:   user,
		logger: logger,
	}
}

func (l *LibService) CreateAuthor(ctx context.Context, author models.Author) error {
	return l.Author.CreateAuthor(ctx, author)
}

func (l *LibService) CreateUser(ctx context.Context, user models.User) error {
	return l.User.CreateUser(ctx, user)
}

func (l *LibService) CreateBook(ctx context.Context, book models.Book) error {
	return l.Book.CreateBook(ctx, book)
}

func (l *LibService) RentBook(ctx context.Context, bookID, userID string) error {

	bookIDInt, err := checkAndConvertId(bookID)
	if err != nil {
		return err
	}
	userIDInt, err := checkAndConvertId(userID)
	if err != nil {
		return err
	}
	book, err := l.Book.GetBookByID(ctx, bookIDInt)
	if err != nil {
		return err
	}
	if !book.IsAvailable() {
		return fmt.Errorf("book is not available")
	}

	book.BookTheBook(userIDInt)

	err = l.Book.UpdateBook(ctx, book)
	if err != nil {
		return err
	}

	return nil
}

func checkAndConvertId(id string) (int, error) {
	i, err := strconv.Atoi(id)
	if err != nil || i <= 0 || id == "" {
		return 0, fmt.Errorf("invalid id: %s", id)
	}
	return i, nil
}

func (l *LibService) ReturnBook(ctx context.Context, bookID, userID string) error {
	bookIDInt, err := checkAndConvertId(bookID)
	if err != nil {
		return err
	}
	userIDInt, err := checkAndConvertId(userID)
	if err != nil {
		return err
	}
	book, err := l.Book.GetBookByID(ctx, bookIDInt)
	if err != nil {
		return err
	}

	if !book.IsRentedBy(userIDInt) {
		return fmt.Errorf("book is not rented by this user")
	}
	book.ReturnTheBook()
	err = l.Book.UpdateBook(ctx, book)
	if err != nil {
		return err
	}

	return nil
}

func (l *LibService) GetAllAuthors(ctx context.Context) (out []models.Author, err error) {
	return l.Author.GetAllAuthors(ctx)
}

func (l *LibService) GetAllUsers(ctx context.Context) (out []models.User, err error) {
	return l.User.GetAllUsers(ctx)
}

func (l *LibService) GetAllBooks(ctx context.Context) (out []models.Book, err error) {
	return l.Book.GetAllBooks(ctx)
}
