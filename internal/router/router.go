package router

import (
	"github.com/go-chi/chi"
	httpSwagger "github.com/swaggo/http-swagger"
	_ "gitlab/library/docs"
	"gitlab/library/internal/infrastructure/component"
	"gitlab/library/internal/modules"
	"net/http"
)

func NewApiRouter(controllers *modules.Controllers, components *component.Components) http.Handler {
	r := chi.NewRouter()
	lib := controllers.Lib

	r.Get("/swagger/*", httpSwagger.Handler(
		httpSwagger.URL("http://localhost:8080/swagger/doc.json"),
	))

	// user, all routes are public
	r.Group(func(r chi.Router) {
		r.Route("/library", func(r chi.Router) {
			r.Post("/author", lib.AddAuthor)
			r.Get("/author", lib.GetAuthors)

			r.Post("/book", lib.AddBook)
			r.Get("/book", lib.GetBooks)

			r.Post("/user", lib.AddUser)
			r.Get("/user", lib.GetUsers)

			r.Post("/rent", lib.RentBook)
			r.Post("/return", lib.ReturnBook)
		})
	})

	return r
}
