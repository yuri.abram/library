package main

import (
	"github.com/joho/godotenv"
	"gitlab/library/config"
	_ "gitlab/library/docs"
	"gitlab/library/internal/infrastructure/logs"
	"gitlab/library/run"
	"os"
)

// @title задача База данных библиотеки
// @version 2.0
// @description  Library
// @host localhost:8080
// @BasePath /

func main() {

	// Загружаем переменные окружения из файла .env
	err := godotenv.Load()
	// Создаем конфигурацию приложения
	conf := config.NewAppConf()
	// Создаем логгер
	logger := logs.NewLogger(conf, os.Stdout)
	if err != nil {
		logger.Fatal("error loading .env file")
	}
	// Инициализируем конфигурацию приложения с логгером
	conf.Init(logger)
	// Создаем инстанс приложения
	app := run.NewApp(conf, logger)

	exitCode := app.
		// Инициализируем приложение
		Bootstrap().
		// Запускаем приложение
		Run()
	os.Exit(exitCode)
}
